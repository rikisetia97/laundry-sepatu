<?php

class Jasa_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get jasa by id
     */
    function get_jasa($id)
    {
        return $this->db->get_where('tb_jasa',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all jasa
     */
    function get_all_jasa()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('tb_jasa')->result_array();
    }

    /*
     * Get all jasa
     */
    function get_all_jasa_limit($limit)
    {
        $this->db->limit($limit);
        return $this->db->get('tb_jasa')->result_array();
    }    
        
    /*
     * function to add new jasa
     */
    function add_jasa($params)
    {
        $this->db->insert('tb_jasa',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update jasa
     */
    function update_jasa($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('tb_jasa',$params);
    }
    
    /*
     * function to delete jasa
     */
    function delete_jasa($id)
    {
        return $this->db->delete('tb_jasa',array('id'=>$id));
    }
}
