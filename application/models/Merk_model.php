<?php

 
class Merk_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get merk by id
     */
    function get_merk($id)
    {
        return $this->db->get_where('tb_merk',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all merk
     */
    function get_all_merk()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('tb_merk')->result_array();
    }
        
    /*
     * function to add new merk
     */
    function add_merk($params)
    {
        $this->db->insert('tb_merk',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update merk
     */
    function update_merk($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('tb_merk',$params);
    }
    
    /*
     * function to delete merk
     */
    function delete_merk($id)
    {
        return $this->db->delete('tb_merk',array('id'=>$id));
    }
}
