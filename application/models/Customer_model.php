<?php

 
class Customer_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get customer by id
     */
    function get_customer($id)
    {
        return $this->db->get_where('tb_customer',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all customer
     */
    function get_all_customer()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('tb_customer')->result_array();
    }
        
    /*
     * function to add new customer
     */
    function add_customer($params)
    {
        $this->db->insert('tb_customer',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update customer
     */
    function update_customer($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('tb_customer',$params);
    }
    
    /*
     * function to delete customer
     */
    function delete_customer($id)
    {
        return $this->db->delete('tb_customer',array('id'=>$id));
    }
}
