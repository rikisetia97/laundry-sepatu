<?php

 
class Transaksi_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get transaksi by id
     */
    function get_transaksi($id)
    {
        return $this->db->get_where('tb_transaksi',array('id'=>$id))->row_array();
    }
    

    /*
     * Get transaksi by customer
     */
    function get_transaksi_user($customer)
    {
        $this->db->join('tb_jasa', 'tb_jasa.id=tb_transaksi.id_jasa');
        return $this->db->get_where('tb_transaksi',array('id_customer'=>$customer))->result_array();
    }
        
    /*
     * Get all transaksi
     */
    function get_all_transaksi()
    {
        $this->db->select('t.id,t.kode_cari,t.jumlah,t.total,t.status,t.biaya_tambahan,t.pesanan_dibuat,j.jasa,j.biaya,c.nama,c.no_hp');
        $this->db->order_by('pesanan_dibuat', 'desc');
        $this->db->join('tb_jasa j', 'j.id=t.id_jasa');
        $this->db->join('tb_customer c', 'c.id=t.id_customer');
        return $this->db->get('tb_transaksi t')->result_array();
    }

    /*
     * Get all transaksi by status
     */
    function get_transaksi_status($status)
    {
        $this->db->select('t.id,t.kode_cari,t.jumlah,t.total,t.status,t.biaya_tambahan,t.pesanan_dibuat,j.jasa,j.biaya,c.nama,c.no_hp');
        $this->db->order_by('pesanan_dibuat', 'desc');
        $this->db->join('tb_jasa j', 'j.id=t.id_jasa');
        $this->db->join('tb_customer c', 'c.id=t.id_customer');
        $this->db->where('t.status', $status);
        return $this->db->get('tb_transaksi t')->result_array();
    }
        
    /*
     * function to add new transaksi
     */
    function add_transaksi($params)
    {
        $this->db->insert('tb_transaksi',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update status transaksi
     */
    function update_status($id)
    {
        $this->db->set('status',1);
        $this->db->where('id',$id);
        return $this->db->update('tb_transaksi');
    }
    
    /*
     * function to delete transaksi
     */
    function delete_transaksi($id)
    {
        return $this->db->delete('tb_transaksi',array('id'=>$id));
    }

    /*
     * function to check transaksi
     */
    function cek_transaksi($kode_cari)
    {
        $this->db->join('tb_jasa j', 'j.id=t.id_jasa');
        $this->db->join('tb_customer c', 'c.id=t.id_customer');
        return $this->db->get_where('tb_transaksi t',array('t.kode_cari'=>$kode_cari))->row();
    }
}
