<?php

class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get admin by id
     */
    function get_admin($id)
    {
        return $this->db->get_where('tb_admin',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all admin
     */
    function get_all_admin()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('tb_admin')->result_array();
    }
        
    /*
     * function to add new admin
     */
    function add_admin($params)
    {
        $this->db->insert('tb_admin',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update admin
     */
    function update_admin($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('tb_admin',$params);
    }

    /*
     * function to update admin
     */
    function update_admin_pass($id,$password)
    {
        $this->db->set('password',$password);
        $this->db->where('id',$id);
        return $this->db->update('tb_admin');
    }

    /*
     * function to update admin
     */
    function check_password($id,$password)
    {
        return $this->db->get_where('tb_admin', ['id' => $id, 'password' => $password])->row();
    }
    
    /*
     * function to delete admin
     */
    function delete_admin($id)
    {
        return $this->db->delete('tb_admin',array('id'=>$id));
    }
}
