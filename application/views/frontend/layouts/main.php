<!DOCTYPE html>
<html lang="en">


<head>
    <title><?=$this->db->get_where('tb_pengaturan', ['jenis' => 'nama_website'])->row()->value;?></title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Calling Favicon -->
    <link rel="shortcut icon" href="<?=base_url();?>resources/admin/dist/img/AdminLTELogo.png">
    <!-- Calling Favicon -->
    <!-- ********************* -->
    <!-- CSS files -->
    <link rel="stylesheet" href="<?=site_url('resources/frontend/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/animate.css">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/all.min.css">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/nice-select.css">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/magnific-popup.css">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/meanmenu.css" media="all">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/twentytwenty.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/default.css">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/style.css">
    <link rel="stylesheet" class="color-changing" href="<?=base_url();?>resources/frontend/css/color.css">
    <link rel="stylesheet" href="<?=base_url();?>resources/frontend/css/responsive.css">
    <!-- End CSS files -->
    <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    
    <script src="<?=base_url();?>resources/frontend/js/jquery-1.12.4.min.js"></script>

    <!-- JS Files end -->
</head>

<body>
    <!-- Preloader start -->
    <div class="loader-out flex-center">
        <div class="loader"></div>
    </div>
    <!-- Preloader end -->
    <header>
        <div class="topheader">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 d-none d-lg-block">
                        <div class="left-head">
                            <a href="#" class="brand-link">
                                <img src="<?=base_url();?>resources/admin/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8; width: 60px; height: 60px">
                                <span class="text-dark" style="font-size: 20px; margin-left: -5px;"><?=$this->db->get_where('tb_pengaturan', ['jenis' => 'nama_website'])->row()->value;?></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-4 text-left text-lg-center">
                    </div>
                    <div class="col-lg-6 col-12 text-right">
                        <div class="right-head">
                            <span class="fs-15 f-500"><i class="fas fa-phone blue mr-10"></i><?=$this->db->get_where('tb_pengaturan', ['jenis' => 'nomor_hp'])->row()->value;?></span>
                            <a href="https://wa.me/<?=$this->db->get_where('tb_pengaturan', ['jenis' => 'nomor_hp'])->row()->value;?>" class="btn btn-secondary btn-md ml-20"><i class="fa fa-envelope"></i> Kirim Pesan</a>
                            <?php if (empty($this->session->userdata('sess_user'))) { ?>
                                <a href="#" class="btn btn-primary btn-md ml-20" data-toggle="modal" data-target="#sign-modal"><i class="fa fa-sign-in-alt"></i> Masuk/Daftar</a>
                            <?php }else{ ?> 
                              <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <?=$this->session->userdata('sess_user')['nama'];?></a>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="<?=base_url('services/riwayat');?>">Riwayat Pesanan</a>
                                <a class="dropdown-item" href="<?=base_url('auth/logout');?>">Keluar</a>
                            </div>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-head bg-black">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-9">
                    <div class="header-1">
                        <nav class="main-menu-1 menu-4">
                            <div class="d-flex align-items-center">
                                <ul>
                                    <li>
                                        <a href="<?=base_url('.');?>" <?php if($this->uri->segment(1) == 'dashboard' || $this->uri->segment(1) == ''){ ?>class="active"<?php } ?>>Beranda <span>+</span></a>
                                    </li>
                                    <li> <a href="<?=base_url('services');?>" <?php if($this->uri->segment(1) == 'services'){ ?>class="active"<?php } ?>>Layanan Kami<span>+</span></a>
                                        <ul class="submenu">
                                            <?php 
                                            $data_service = $this->db->get('tb_jasa')->result();
                                            foreach ($data_service as $item) { ?>
                                                <li><a href="<?=base_url('services/checkout/'.$item->id);?>"><?=$item->jasa;?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="<?=base_url('about');?>" <?php if($this->uri->segment(1) == 'about'){ ?>class="active"<?php } ?>>Tentang Kami <span>+</span></a>
                                    </li>
                                    <li>
                                        <a href="<?=base_url('faq');?>" <?php if($this->uri->segment(1) == 'faq'){ ?>class="active"<?php } ?>>FAQ <span>+</span></a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mobile-menu-4"></div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- The search Modal start -->
<div class="search-popup modal" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="#">
                    <div class="form-group relative">
                        <input type="text" class="form-control input-search" id="search" placeholder="Search here..."> <i class="fas fa-search blue transform-v-center"></i>
                    </div>
                </form>
            </div>
        </div>
    </div> <i class="fas fa-times close-search-modal" data-dismiss="modal"></i>
</div>
<!-- The search Modal end -->
<!-- header end -->


<?php                    
if(isset($_view) && $_view)
    $this->load->view($_view);
?>     


<!-- footer start -->
<footer class="footer-1" data-overlay="8" style="">
    <div class="container">
        <div class="row z-5">
            <div class="col-lg-12">
                <div class="hr-2 bg-light-white mb-40 opacity-1"></div>
            </div>
            <div class="col-lg-8">
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url();?>resources/frontend/img/favicon.png">
                <p class="mb-0">© <?=$this->db->get_where('tb_pengaturan', ['jenis' => 'nama_website'])->row()->value;?>  2020 Allright Reserved</p>
            </div>
            <div class="col-lg-4 text-center">
                <a href="#" class="scroll-btn bg-blue"><i class="fas fa-arrow-up"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- footer end -->
<div class="modal fade" id="sign-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Login Form</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="POST" action="<?=base_url('auth/check');?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nomor HP</label>
                        <input type="text" class="form-control" name="no_hp" placeholder="Input Nomor HP" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Input Password" required="required">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" onclick="register()" class="btn btn-primary">Daftar Akun Baru</button>
                    <button type="submit" class="btn btn-success">Masuk</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="register-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Register Form</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="POST" action="<?=base_url('auth/register');?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nomor HP</label>
                        <input type="text" class="form-control" name="no_hp" placeholder="Input Nomor HP" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" class="form-control" name="nama" placeholder="Input Nama Lengkap" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tgl_lahir" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Input Password" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">Alamat Lengkap</label>
                        <textarea class="form-control" name="alamat" required="required"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-primary" onclick="login()">Sudah punya akun?</button>
                    <button type="submit" class="btn btn-success">Daftar Sekarang</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- JS Files -->

<script src="<?=base_url();?>resources/frontend/js/modernizr-3.5.0.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/owl.carousel.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/jquery.magnific-popup.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/jquery.nice-select.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/jquery.waypoints.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/jquery.counterup.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/jquery.countdown.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/lightslider.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/wow.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/isotope.pkgd.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/jquery.meanmenu.min.js"></script>
<script src="<?=base_url();?>resources/admin/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/tilt.jquery.min.js"></script>
<script src="<?=base_url();?>resources/frontend/js/jquery.event.move.js"></script>
<script src="<?=base_url();?>resources/frontend/js/jquery.twentytwenty.js"></script>
<script src="<?=base_url();?>resources/frontend/js/main.js"></script>


<script src="<?=base_url();?>resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>resources/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>

<script>
    function register() {
        $('#sign-modal').modal('hide');
        setTimeout(() => {  $('#register-modal').modal('show'); }, 500);

    }
    function login() {
        $('#register-modal').modal('hide');
        setTimeout(() => {  $('#sign-modal').modal('show'); }, 500);

    }

    var success_msg = "<?=$this->session->flashdata('success');?>";
    var failed_msg = "<?=$this->session->flashdata('failed');?>";
    $(function () {

      if (success_msg) {
        Swal.fire({
          icon: 'success',
          title: success_msg
      })
    }
    if (failed_msg) {
        Swal.fire({
          icon: 'error',
          title: failed_msg
      })
    }

});

    $('#datatable').DataTable();
</script>
</body>


</html>