<!-- Services start -->
<section class="service pt-50 pb-70 bg-light-white">
    <div class="container">
        <div class="row">
            <?php foreach ($jasa as $item) { ?>
                <div class="col-lg-3 col-md-6 wow fadeInUp">
                    <div class="service-each shadow-2 mb-30 transition-4 text-center">
                        <a href="<?=base_url('services/checkout/'.$item['id']);?>" class="black">
                            <div class="service-icn bg-light-white flex-center">
                                <img src="<?=base_url();?>resources/frontend/img/service/1.png" alt="">
                            </div>
                            <div class="service-text">
                                <h3 class="fs-20 f-700 mb-10"><?=$item['jasa'];?></h3>
                                <p class="mb-0"><?=$item['deskripsi'];?></p>
                                <span class="line-servcie transition-4 bg-blue mt-5"></span>
                            </div>
                        </a>
                    </div>
                </div>
            <?php } ?>


        </div>
    </div>
</section>
    <!-- Services end -->