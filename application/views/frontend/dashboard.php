
<!-- Banner start -->
<section class="banner-type-4 flex-center" style="background-image: url('resources/frontend/img/banner/banner-3a.jpg');" data-overlay="9">
    <div class="container">
        <div class="row z-5">
            <div class="col-lg-10 offset-lg-1 text-center">
                <div class="banner-content-4 white">
                    <h4 class="fs-21 f-400 mb-5">Pantau barang Anda dengan cara yang mudah, tanpa perlu datang langsung ke Kami.</h4>
                    <h1 class="f-900 mb-25">Cek Status Pesanan Anda</h1>
                    <div class="banner-form-4">
                        <!-- <form action="<?=base_url('dashboard/cek_status');?>" method="POST"> -->
                            <div class=" d-flex flex-column flex-md-row">
                                <input type="text" class="form-control input-white shadow-2 ml-15 ml-xs-00" placeholder="Masukkan Kode Cari" id="kode_cari">
                                <button type="button" onclick="cari()" class="btn btn-black ml-15 ml-xs-00 mt-xs-15">Go</button>
                            </div>
                            <!-- </form> -->
                        </div>
                        <ul class="banner-check-list white mt-25">
                            <li><i class="far fa-check-circle white"></i>Terpercaya & Tercepat</li>
                            <li><i class="far fa-check-circle white"></i>Aman & Bergaransi</li>
                            <li><i class="far fa-check-circle white"></i>Biaya Murah</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner end -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <p class="sub-head blue f-700 mb-5 mt-10"><span>............</span>LAYANAN TERBAIK KAMI<span>............</span></p>
            </div>
        </div>
        <div class="row">
           <?php foreach ($jasa as $item) { ?>
            <div class="col-lg-3 col-md-6 wow fadeInUp">
                <div class="service-each shadow-2 mb-30 transition-4 text-center">
                    <a href="<?=base_url('services/checkout/'.$item['id']);?>" class="black">
                        <div class="service-icn bg-light-white flex-center">
                            <img src="<?=base_url();?>resources/frontend/img/service/1.png" alt="">
                        </div>
                        <div class="service-text">
                            <h3 class="fs-20 f-700 mb-10"><?=$item['jasa'];?></h3>
                            <p class="mb-0"><?=$item['deskripsi'];?></p>
                            <span class="line-servcie transition-4 bg-blue mt-5"></span>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="row align-items-center mb-20">
        <div class="col-md-8 text-center text-md-left">
        </div>
        <div class="col-md-4 text-center text-md-right">
            <a href="<?=base_url('services');?>" class="btn btn-blue shine-btn btn-md">Lihat Semua <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>


<div class="modal fade" id="kode-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Status Kode Cari</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Kode Cari</label>
                    <input type="text" class="form-control" readonly="readonly" id="kode">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Customer</label>
                            <input type="text" class="form-control" readonly="readonly" id="nama_customer">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Jasa Laundry</label>
                            <input type="text" class="form-control" readonly="readonly" id="jasa">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Biaya Jasa</label>
                            <input type="text" class="form-control" readonly="readonly" id="biaya">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Biaya Tambahan</label>
                            <input type="text" class="form-control" readonly="readonly" id="biaya_tambahan">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Biaya Total</label>
                            <input type="text" class="form-control" readonly="readonly" id="total">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Tanggal Pesan</label>
                    <input type="text" class="form-control" readonly="readonly" id="pesanan_dibuat">
                </div>
                <div class="form-group">
                    <label for="">Status Pesanan</label>
                    <input type="text" class="form-control" readonly="readonly" id="status">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-block" class="close" data-dismiss="modal" aria-label="Close">Tutup</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    function cari() {
        kode_cari = $('#kode_cari').val()
        $.ajax({
            url: "dashboard/get_transaksi/"+kode_cari, 
            type: "GET",
            success: function(result){
                var obj = JSON.parse(result);
                if (obj != null) {
                    $("#kode").val(obj['kode_cari']);
                    $("#nama_customer").val(obj['nama']);
                    $("#jasa").val(obj['jasa']);
                    $("#biaya").val(obj['biaya']);
                    $("#biaya_tambahan").val(obj['biaya_tambahan']);
                    $("#total").val(obj['total']);
                    $("#pesanan_dibuat").val(obj['pesanan_dibuat']);
                    if (obj['status'] == 1) {
                        obj['status'] = 'Selesai';
                    }else{
                        obj['status'] = 'Dalam Proses';
                    }
                    $("#status").val(obj['status']);
                    $('#kode-modal').modal('show');
                }else{
                    Swal.fire({
                      icon: 'error',
                      title: 'Kode Cari tidak ditemukan'
                  })
                }
            },
            error: function (jqXHR, exception) {
                Swal.fire({
                  icon: 'error',
                  title: 'Kode Cari tidak boleh kosong'
              })
            }
        })

    }
</script>