<section class="faq pt-90 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-7">
                <h3 class="fs-22 mb-15 f-700">FAQ</h3>
                <div class="faq-boxes">
                    <div id="accordion">
                        <?php foreach ($faq as $item) { ?>
                            <div class="card">
                                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne<?=$item->id;?>" aria-expanded="true" aria-controls="collapseOne<?=$item->id;?>" role="button">
                                    <h5 class="mb-0">
                                        <?=$item->pertanyaan;?><i class="fas fa-plus"></i>
                                    </h5>
                                </div>
                                <div id="collapseOne<?=$item->id;?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                    <div class="card-body">
                                        <?=$item->jawaban;?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>