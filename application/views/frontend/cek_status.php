
    <!-- Banner start -->
    <section class="banner-type-4 flex-center" style="background-image: url('resources/frontend/img/banner/banner-3a.jpg');" data-overlay="8">
        <div class="container">
            <div class="row z-5">
                <div class="col-lg-10 offset-lg-1 text-center">
                    <div class="banner-content-4 white">
                        <a href="https://www.youtube.com/watch?v=qtQgbdmIO30" class="play-btn-white popup-video mb-30">
                            <i class="fas fa-play"></i>
                        </a>
                        <h4 class="fs-21 f-400 mb-5">Pantau barang Anda dengan cara yang mudah, tanpa perlu datang langsung ke Kami.</h4>
                        <h1 class="f-900 mb-25">Cek Status Pesanan Anda</h1>
                        <div class="banner-form-4">
                            <form action="<?=base_url('dashboard/cek_status');?>" method="POST">
                                <div class=" d-flex flex-column flex-md-row">
                                    <input type="text" class="form-control input-white shadow-2 ml-15 ml-xs-00" placeholder="Masukkan Kode Cari" name="kode_cari">
                                    <button type="submit" class="btn btn-black ml-15 ml-xs-00 mt-xs-15">Go</button>
                                </div>
                            </form>
                        </div>
                        <ul class="banner-check-list white mt-25">
                            <li><i class="far fa-check-circle white"></i>Terpercaya & Tercepat</li>
                            <li><i class="far fa-check-circle white"></i>Aman & Bergaransi</li>
                            <li><i class="far fa-check-circle white"></i>Biaya Murah</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner end -->
    <!-- features start -->
    <section class="features relative o-hidden pt-100 pb-55">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="each-feature">
                        <img src="<?=base_url();?>resources/frontend/img/icons/fe1.png" alt="">
                        <h3 class="fs-20 f-700 mt-20 mb-10">Terpercaya & Tercepat</h3>
                        <p class="mb-0">Ut enim ad minim veniam</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="each-feature">
                        <img src="<?=base_url();?>resources/frontend/img/icons/fe2.png" alt="">
                        <h3 class="fs-20 f-700 mt-20 mb-10">Aman & Bergaransi</h3>
                        <p class="mb-0">Ut enim ad minim veniam</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="each-feature">
                        <img src="<?=base_url();?>resources/frontend/img/icons/fe3.png" alt="">
                        <h3 class="fs-20 f-700 mt-20 mb-10">Professional</h3>
                        <p class="mb-0">Ut enim ad minim veniam</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="each-feature">
                        <img src="<?=base_url();?>resources/frontend/img/icons/fe4.png" alt="">
                        <h3 class="fs-20 f-700 mt-20 mb-10">Biaya Murah</h3>
                        <p class="mb-0">Ut enim ad minim veniam</p>
                    </div>
                </div>
            </div>
        </div>
        <img src="<?=base_url();?>resources/frontend/img/other/clean.png" class="feature-bg-icon2" alt="">
    </section>
    <!-- features end -->
    
    <!-- why choose us start -->
    <section class="why-us">
        <div class="container-fluid p-0">
            <div class="row no-gutters z-5">
                <div class="col-lg-6 wow fadeInLeft">
                    <div class="img-choose-us bg-cover h-100" style="background-image: url('resources/frontend/img/bg/why-3.jpg');"></div>
                </div>
                <div class="col-lg-6  wow fadeInRight">
                    <div class="why-us-content">
                        <p class="sub-head blue f-700 mb-5">WHY CHOOSE US<span>............</span></p>
                        <h1 class="mb-10">Your Happiness Is Our Priority</h1>
                        <p class="mb-20">Nunc tempor velit molestie arcu malesuada accumsan. Nam sollicitudin a orci quis vestibulum. Sed fringilla neque eu ultrices consequat. Nunc at varius ante. Suspendisse imperdiet, velit at tempor aliquet, augue nunc ullamcorper sem, eget imperdiet nunc leo eget mi.</p>
                        <ul class="check-list2 half-list mb-10">
                            <li>
                                <img src="<?=base_url();?>resources/frontend/img/icons/check.png" alt="">
                                <h5 class="fs-16 lh-14">Highly Rated & Esteemed</h5>
                                <p>Ut enim ad minim veniam, quis</p>
                            </li>
                            <li>
                                <img src="<?=base_url();?>resources/frontend/img/icons/check.png" alt="">
                                <h5 class="fs-16 lh-14">Insured and Bonded</h5>
                                <p>Ut enim ad minim veniam, quis</p>
                            </li>
                            <li>
                                <img src="<?=base_url();?>resources/frontend/img/icons/check.png" alt="">
                                <h5 class="fs-16 lh-14">Trusted Professionals</h5>
                                <p>Ut enim ad minim veniam, quis</p>
                            </li>
                            <li>
                                <img src="<?=base_url();?>resources/frontend/img/icons/check.png" alt="">
                                <h5 class="fs-16 lh-14">Quality Service</h5>
                                <p>Ut enim ad minim veniam, quis</p>
                            </li>
                        </ul>
                        <a href="#" class="btn btn-blue shine-btn mr-20">Book Now</a>
                        <a href="#" class="btn btn-blue-border mt-xs-15">Contact Us</a>
                        <img src="<?=base_url();?>resources/frontend/img/other/clean.png" class="why-bg-icon" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- why choose us end -->