<div class="content-wrapper" style="min-height: 543px;">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-12">
					<div class="card">
						<!-- /.card-header -->
						<div class="card-header">
							<h3 class="card-title mt-2">Daftar Riwayat Pemesanan</h3>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								
								<table id="datatable" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>No.</th>
											<th>Kode Cari</th>
											<th>Pesanan Dibuat</th>
											<th>Jasa</th>
											<th>Biaya Jasa</th>
											<th>Biaya Tambahan</th>
											<th>Jumlah</th>
											<th>Total</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<?php $i = 1; foreach ($pesanan as $item) { ?>
											<tr>
												<td><?=$i++;?></td>
												<td><?=$item['kode_cari'];?></td>
												<td><?=$item['pesanan_dibuat'];?></td>
												<td><?=$item['jasa'];?></td>
												<td><?=$item['biaya'];?></td>
												<td><?=$item['biaya_tambahan'];?></td>
												<td><?=$item['jumlah'];?></td>
												<td><?=$item['total'];?></td>
												<td>
													<span class="badge badge-<?php if($item['status'] == 0) { echo "warning"; } else { echo "success"; } ?>"> <?php if($item['status'] == 0) {echo "Dalam Proses"; } else {echo "Selesai"; } ?> </span>
												</td>
											</tr>
										<?php } ?>
									</tbody>
									<tfoot>
										<tr>
											<th>No.</th>
											<th>Kode Cari</th>
											<th>Pesanan Dibuat</th>
											<th>Jasa</th>
											<th>Biaya Jasa</th>
											<th>Biaya Tambahan</th>
											<th>Jumlah</th>
											<th>Total</th>
											<th>Status</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
						<!-- /.row -->
					</div><!-- /.container-fluid -->
				</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->
		</div>

		<div class="modal fade" id="modal-add" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Tambah Jasa</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form method="POST" action="<?=base_url('admin/jasa/tambah');?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Nama Jasa</label>
								<input type="text" class="form-control" name="jasa" placeholder="Input Nama Jasa" required="required">
							</div>
							<div class="form-group">
								<label for="">Biaya Jasa</label>
								<input type="text" class="form-control" name="biaya" placeholder="Input Biaya Jasa" required="required">
							</div>
							<div class="form-group">
								<label for="">Deskripsi</label>
								<textarea class="form-control" name="deskripsi"></textarea>
							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Simpan Data</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<div class="modal fade" id="modal-edit" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Edit Kustomer</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form method="POST" action="<?=base_url('admin/jasa/edit');?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Nama Jasa</label>
								<input type="hidden" id="id" name="id">
								<input type="text" class="form-control" id="jasa" name="jasa" placeholder="Input Nama Jasa" required="required">
							</div>
							<div class="form-group">
								<label for="">Biaya Jasa</label>
								<input type="number" class="form-control" id="biaya" name="biaya" placeholder="Input Biaya Jasa" required="required">
							</div>
							<div class="form-group">
								<label for="">Deskripsi</label>
								<textarea class="form-control" id="deskripsi" name="deskripsi"></textarea>
							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Simpan Perubahan</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<script>
			function hapus(id) {
				Swal.fire({
					title: 'Apakah anda yakin?',
					text: "Anda akan menghapus data jasa berikut?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Hapus saja!'
				}).then((result) => {
					if (result.isConfirmed) {
						$.ajax({
							url: "jasa/hapus_jasa/"+id, 
							type: "GET",
							success: function(result){
								Swal.fire(
									'Dihapus!',
									'Data berhasil dihapus.',
									'success'
									).then(function(result) {
										location.reload();
									});
								}
							})
					}
				})
			}

			function edit(id) {
				$.ajax({
					url: "jasa/detail_jasa/"+id, 
					type: "GET",
					success: function(result){
						var obj = JSON.parse(result);
						console.log(obj)
						$("#id").val(obj['id']);
						$("#jasa").val(obj['jasa']);
						$("#biaya").val(obj['biaya']);
						$("#deskripsi").val(obj['deskripsi']);
						$("#modal-edit").modal('show');
					}
				})
			}
		</script>