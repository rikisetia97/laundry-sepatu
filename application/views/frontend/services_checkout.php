<!-- Services start -->
<section class="service pt-50 pb-70 bg-light-white">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-5">
                <div class="right-box bg-blue mb-30">
                 <div class="service-each shadow-2 mb-30 transition-4 text-center">
                    <a href="#" class="black">
                        <div class="service-icn bg-light-white flex-center">
                            <img src="<?=base_url();?>resources/frontend/img/service/1.png" alt="">
                        </div>
                        <div class="service-text">
                            <h3 class="fs-20 f-700 mb-10"><?=$jasa['jasa'];?></h3>
                            <p class="mb-0"><?=$jasa['deskripsi'];?></p>
                            <span class="line-servcie transition-4 bg-blue mt-5"></span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="servvice-link-list mb-30">
                <h6 class="mb-2">Pilih Layanan Kami :</h6>
                <ul>
                    <?php foreach ($list_jasa as $item) { ?>
                        <li <?php if($item['id'] == $this->uri->segment(3)) { ?> class="active" <?php } ?>><a href="<?php if($item['id'] != $this->uri->segment(3)) { ?><?=base_url('services/checkout/'.$item['id']);?><?php } ?>" id="jasa_<?=$item['id'];?>"><i class="fas fa-chevron-right"></i><?=$item['jasa'];?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="col-xl-8 col-lg-7">
            <div class="calculation">
                <h3 class="f-700 fs-30 mb-10">Kalkulasi Biaya Laundry Anda</h3>
                <form action="<?=base_url('services/save_order');?>" method="POST" id="form">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="">Jenis Layanan Customer</label>
                                <select class="form-control" id="jenis_layanan_customer" name="id_jasa">
                                    <option value="" selected="selected" disabled="disabled">Pilih Jenis Layanan Customer</option>
                                    <option value="">Ambil/Jemput Sendiri</option>
                                    <option value="2">Ambil/Jemput Petugas</option>
                                    <option value="3">Antar Sendiri (Dijemput Petugas)</option>
                                    <option value="4">Jemput Sendiri (Diantarkan Petugas)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                           <div class="form-group">
                            <label for="">Jumlah Pakaian/Sepatu :</label>
                            <input type="hidden" id="biaya_tambahan" name="biaya_tambahan">
                            <input type="hidden" id="total" name="total">
                            <input type="number" id="jumlah" class="form-control" name="jumlah" placeholder="Masukkan Jumlah Pakaian/Sepatu">
                        </div>
                    </div>
                </div>
            </div>

            <div class="hr-1 bg-black opacity-1 mb-30 mt-5"></div>
            <div class="total-cost-calc text-center">
                <p class="mb-0">Estimasi Total Biaya Anda</p>
                <h2 class="mb-10 blue f-500">Rp. <span id="total_text">0</span></h2>
            </div>
            <div class="hr-1 bg-black opacity-1 mb-35 mt-30"></div>
            <div class="row">
              <div class="col-lg-6 mt-2">
                <?php if (empty($this->session->userdata('sess_user'))) { ?> <button type="button" onclick="login()" class="btn btn-primary btn-block"><i class="fa fa-sign-in-alt"></i> Masuk/Daftar</button><?php }?>
                
            </div>
            <div class="col-lg-6 mt-2">
                <button type="button" class="btn btn-primary btn-block" <?php if (empty($this->session->userdata('sess_user'))) { ?> disabled="disabled"<?php }else{ ?> id="btn_submit" <?php }?>><i class="fa fa-paper-plane"></i> Order sekarang</button>
            </div>

        </div>
    </form>
</div>
</div>
</div>
</section>

<script type="text/javascript">
    $('#btn_submit').click(function(){

        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan memesanan jasa berikut?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Pesan!'
        }).then((result) => {
            if (result.isConfirmed) {
                $('#form').submit()
            }
        })


    })

    var jasa = "<?=$jasa['biaya'];?>";
    var total = 0;
    $('#total_text').html(jasa)
    $("#jenis_layanan_customer").change(function() {
        var jenis = $("#jenis_layanan_customer").val();
        if (jenis != null) {
            tambahan = 0;
            if (jenis == 2) {
                $.ajax({
                    url: "../get_pengaturan/biaya_antar", 
                    type: "GET",
                    success: function(result){
                        jml = $("#jumlah").val();
                        var obj = JSON.parse(result);
                        tambahan += parseInt(obj['value']);
                        total = jasa * jml + parseInt(tambahan);
                        $('#total_text').html(total)
                        $("#biaya_tambahan").val(tambahan);
                        $("#total").val(total);
                    }
                })
                $.ajax({
                    url: "../get_pengaturan/biaya_jemput", 
                    type: "GET",
                    success: function(result){
                        jml = $("#jumlah").val();
                        var obj = JSON.parse(result);
                        tambahan += parseInt(obj['value']);
                        total = jasa * jml + parseInt(tambahan);
                        $('#total_text').html(total)
                        $("#biaya_tambahan").val(tambahan);
                        $("#total").val(total);
                    }
                })
            }else if (jenis == 3) {
                $.ajax({
                    url: "../get_pengaturan/biaya_jemput", 
                    type: "GET",
                    success: function(result){
                        jml = $("#jumlah").val();
                        var obj = JSON.parse(result);
                        tambahan += parseInt(obj['value']);
                        total = jasa * jml + parseInt(tambahan);
                        $('#total_text').html(total)
                        $("#biaya_tambahan").val(tambahan);
                        $("#total").val(total);
                    }
                })
            }else if (jenis == 4) {
                $.ajax({
                    url: "../get_pengaturan/biaya_antar", 
                    type: "GET",
                    success: function(result){
                        jml = $("#jumlah").val();
                        var obj = JSON.parse(result);
                        tambahan += parseInt(obj['value']);
                        total = jasa * jml + parseInt(tambahan);
                        $('#total_text').html(total)
                        $("#biaya_tambahan").val(tambahan);
                        $("#total").val(total);
                    }
                })
            }
        }else{
            jml = $("#jumlah").val();
            total = jasa * jml;

            $('#total_text').html(total)
            $("#biaya_tambahan").val(0);
            $("#total").val(total);
        }
    })

    $("#jumlah").on("keyup change", function() {
        jml = $("#jumlah").val();
        tambahan = $("#biaya_tambahan").val();
        total = jasa * jml + parseInt(tambahan);
        $('#total_text').html(total)
        $("#biaya_tambahan").val(tambahan);
        $("#total").val(total);
    })
</script>