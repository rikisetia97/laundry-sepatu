<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin | <?=$this->db->get_where('tb_pengaturan', ['jenis' => 'nama_website'])->row()->value;?></title>
  <link rel="shortcut icon" href="<?=base_url();?>resources/admin/dist/img/AdminLTELogo.png">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=base_url();?>resources/admin/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?=base_url();?>resources/admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <script src="<?=base_url();?>resources/admin/plugins/jquery/jquery.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url();?>resources/admin/dist/js/adminlte.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/select2/js/select2.full.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?=base_url();?>resources/admin//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/jszip/jszip.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/pdfmake/pdfmake.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/pdfmake/vfs_fonts.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/sweetalert2/sweetalert2.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/datatables-buttons/js/buttons.print.min.js"></script>
  <script src="<?=base_url();?>resources/admin/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
          </a>
        </li>
      </ul>
    </nav>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <a href="#" class="brand-link">
        <img src="<?=base_url();?>resources/admin/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"><?=$this->db->get_where('tb_pengaturan', ['jenis' => 'nama_website'])->row()->value;?></span>
      </a>
      <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="<?=base_url();?>resources/admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block"><?=$this->session->userdata('sess_admin')['nama_lengkap'];?></a>
          </div>
        </div>
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="<?=base_url('admin/dashboard');?>" class="nav-link <?php if ($this->uri->segment(2) == 'dashboard') { echo "active"; } ?>">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?=base_url('admin/customer');?>" class="nav-link <?php if ($this->uri->segment(2) == 'customer') { echo "active"; } ?>">
                <i class="nav-icon fas fa-users"></i>
                <p>Data Kustomer</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?=base_url('admin/jasa');?>" class="nav-link <?php if ($this->uri->segment(2) == 'jasa') { echo "active"; } ?>">
                <i class="nav-icon fas fa-suitcase"></i>
                <p>Jasa</p>
              </a>
            </li>
            <li class="nav-item  <?php if ($this->uri->segment(2) == 'transaksi') { echo "menu-open"; } ?>">
              <a href="#" class="nav-link <?php if ($this->uri->segment(2) == 'transaksi') { echo "active"; } ?>">
                <i class="nav-icon fas fa-exchange-alt"></i>
                <p>Pesanan <i class="right fas fa-angle-left"></i></p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?=base_url('admin/transaksi/index/semua');?>" class="nav-link <?php if ($this->uri->segment(4) == 'semua') { echo "active"; } ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Semua</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=base_url('admin/transaksi/index/dalam_proses');?>" class="nav-link <?php if ($this->uri->segment(4) == 'dalam_proses') { echo "active"; } ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Dalam Proses</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=base_url('admin/transaksi/index/selesai');?>" class="nav-link <?php if ($this->uri->segment(4) == 'selesai') { echo "active"; } ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Selesai</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item  <?php if ($this->uri->segment(2) == 'pengaturan') { echo "menu-open"; } ?>">
              <a href="#" class="nav-link <?php if ($this->uri->segment(2) == 'pengaturan') { echo "active"; } ?>">
                <i class="nav-icon fas fa-cog"></i>
                <p>Pengaturan <i class="right fas fa-angle-left"></i></p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?=base_url('admin/pengaturan/general');?>" class="nav-link <?php if ($this->uri->segment(3) == 'general') { echo "active"; } ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>General</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=base_url('admin/pengaturan/admin');?>" class="nav-link <?php if ($this->uri->segment(3) == 'admin') { echo "active"; } ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Administrator</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=base_url('admin/pengaturan/ganti_password');?>" class="nav-link <?php if ($this->uri->segment(3) == 'ganti_password') { echo "active"; } ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Ganti Password</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?=base_url('admin/pengaturan/faq');?>" class="nav-link <?php if ($this->uri->segment(3) == 'faq') { echo "active"; } ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Daftar FAQ</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item mt-2">
              <a href="<?=base_url('admin/auth/logout');?>" class="nav-link active" style="background-color: #dc3545">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>Logout</p>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </aside>

    <?php                    
    if(isset($_view) && $_view)
      $this->load->view($_view);
    ?>
    <footer class="main-footer">
      <strong>My Laundry &copy; 2020-2021</strong>
      <div class="float-right d-none d-sm-inline-block">
        <?=date('Y-m-d H:i:s');?>
      </div>
    </footer>
  </div>

  <script>

    var success_msg = "<?=$this->session->flashdata('success');?>";
    var failed_msg = "<?=$this->session->flashdata('failed');?>";
    $(function () {

      if (success_msg) {
        Swal.fire({
          icon: 'success',
          title: success_msg
        })
      }
      if (failed_msg) {
        Swal.fire({
          icon: 'error',
          title: failed_msg
        })
      }
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('.select2').select2({
        theme: 'bootstrap4'
      })

    });
  </script>
</body>
</html>
