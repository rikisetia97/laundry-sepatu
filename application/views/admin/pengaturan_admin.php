<div class="content-wrapper" style="min-height: 543px;">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active">Data Kustomer</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-12">
					<div class="card card-secondary">
						<!-- /.card-header -->
						<div class="card-header">
							<h3 class="card-title mt-2">Data Administrator</h3>
							<button class="float-sm-right btn btn-success" data-toggle="modal" data-target="#modal-add"><i class="fas fa-plus"></i> Tambah Administrator</button>
						</div>
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Username</th>
										<th>Email Address</th>
										<th>Nama Lengkap</th>
										<th>Tanggal Gabung</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($admin as $key) { ?>
										<tr>
											<td><?=$i++;?></td>
											<td><?=$key['username'];?></td>
											<td><?=$key['email'];?></td>
											<td><?=$key['nama_lengkap'];?></td>
											<td><?=$key['created_at'];?></td>
											<td>
												<button class="btn btn-primary" onclick="edit(<?=$key['id'];?>)"><i class="fas fa-edit"></i></button>
											</td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No.</th>
										<th>Username</th>
										<th>Email Address</th>
										<th>Nama Lengkap</th>
										<th>Tanggal Gabung</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.row -->
					</div><!-- /.container-fluid -->
				</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->
		</div>
		<div class="modal fade" id="modal-add" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Tambah Kustomer</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form method="POST" action="<?=base_url('admin/pengaturan/tambah');?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Username</label>
								<input type="text" class="form-control" name="username" placeholder="Input Username" required="required">
							</div>
							<div class="form-group">
								<label for="">Email Address</label>
								<input type="email" class="form-control" name="email" placeholder="Input Email Address" required="required">
							</div>
							<div class="form-group">
								<label for="">Nama Lengkap</label>
								<input type="text" class="form-control" name="nama_lengkap" placeholder="Input Nama Lengkap" required="required">
							</div>
							<div class="form-group">
								<label for="">Password</label>
								<input type="password" class="form-control" name="password" placeholder="Input Password" required="required">
							</div>
							<div class="form-group">
								<label for="">Konfirmasi Password</label>
								<input type="password" class="form-control" name="password" placeholder="Input Password" required="required">
							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Simpan Data</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<div class="modal fade" id="modal-edit" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Edit Administrator</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form method="POST" action="<?=base_url('admin/pengaturan/edit');?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Username</label>
								<input type="hidden" id="id" name="id">
								<input type="text" class="form-control" id="username" name="username" placeholder="Input Username" required="required">
							</div>
							<div class="form-group">
								<label for="">Email Address</label>
								<input type="email" class="form-control" id="email" name="email" placeholder="Input Email Address" required="required">
							</div>
							<div class="form-group">
								<label for="">Nama Lengkap</label>
								<input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Input Nomor HP" required="required">
							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Simpan Perubahan</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<script>
			function edit(id) {
				$.ajax({
					url: "../pengaturan/detail_admin/"+id, 
					type: "GET",
					success: function(result){
						var obj = JSON.parse(result);
						$("#id").val(obj['id']);
						$("#username").val(obj['username']);
						$("#email").val(obj['email']);
						$("#nama_lengkap").val(obj['nama_lengkap']);
						$("#modal-edit").modal('show');
					}
				})
			}
		</script>