<div class="content-wrapper" style="min-height: 543px;">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active">Ganti Password</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-md-12">
					<div class="card card-secondary">
						<div class="card-header">
							<h3 class="card-title">Ganti Password Anda</h3>

							<div class="card-tools">
								<button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
									<i class="fas fa-minus"></i>
								</button>
							</div>
						</div>
						<form method="POST" action="<?=base_url('admin/pengaturan/simpan_password');?>">
						<div class="card-body">
							<div class="form-group">
								<label for="">Password Lama</label>
								<input type="password" class="form-control" name="old_password">
							</div>
							<div class="form-group">
								<label for="">Password Baru</label>
								<input type="password" class="form-control" name="new_password">
							</div>
							<div class="form-group">
								<label for="">Ulangi Password Baru</label>
								<input type="password" class="form-control" name="repeat_new_password">
							</div>
							
						</div>
						<div class="card-footer">
							<button class="btn btn-primary float-sm-right">Simpan Perubahan</button>
						</div>
						</form>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</section>
			<!-- /.content -->
		</div>

