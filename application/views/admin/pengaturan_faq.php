<div class="content-wrapper" style="min-height: 543px;">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active">Daftar FAQ</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-12">
					<div class="card card-secondary">
						<!-- /.card-header -->
						<div class="card-header">
							<h3 class="card-title mt-2">Daftar FAQ</h3>
							<button class="float-sm-right btn btn-success" data-toggle="modal" data-target="#modal-add"><i class="fas fa-plus"></i> Tambah FAQ</button>
						</div>
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Pertanyaan</th>
										<th>Jawaban</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($faq as $key) { ?>
										<tr>
											<td><?=$i++;?></td>
											<td><?=$key['pertanyaan'];?></td>
											<td><?=$key['jawaban'];?></td>
											<td>
												<button class="btn btn-primary" onclick="edit(<?=$key['id'];?>)"><i class="fas fa-edit"></i></button>
												<button class="btn btn-danger" onclick="hapus(<?=$key['id'];?>)"><i class="fas fa-trash"></i></button>
											</td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No.</th>
										<th>Pertanyaan</th>
										<th>Jawaban</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.row -->
					</div><!-- /.container-fluid -->
				</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->
		</div>
		<div class="modal fade" id="modal-add" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Tambah FAQ</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form method="POST" action="<?=base_url('admin/pengaturan/tambah_faq');?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Pertanyaan</label>
								<input type="hidden" id="id" name="id">
								<input type="text" class="form-control" name="pertanyaan" placeholder="Input Username" required="required">
							</div>
							<div class="form-group">
								<label for="">Jawaban</label>
								<textarea class="form-control" name="jawaban"></textarea>
							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Simpan Data</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<div class="modal fade" id="modal-edit" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Edit FAQ</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form method="POST" action="<?=base_url('admin/pengaturan/edit_faq');?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Pertanyaan</label>
								<input type="hidden" id="id" name="id">
								<input type="text" class="form-control" id="pertanyaan" name="pertanyaan" placeholder="Input Username" required="required">
							</div>
							<div class="form-group">
								<label for="">Jawaban</label>
								<textarea class="form-control" name="jawaban" id="jawaban"></textarea>
							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Simpan Perubahan</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<script>
			function hapus(id) {
				Swal.fire({
					title: 'Apakah anda yakin?',
					text: "Anda akan menghapus data FAQ berikut?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Hapus saja!'
				}).then((result) => {
					if (result.isConfirmed) {
						$.ajax({
							url: "../pengaturan/hapus_faq/"+id, 
							type: "GET",
							success: function(result){
								Swal.fire(
									'Dihapus!',
									'Data berhasil dihapus.',
									'success'
									).then(function(result) {
										location.reload();
									});
								}
							})
					}
				})
			}
			function edit(id) {
				$.ajax({
					url: "../pengaturan/detail_faq/"+id, 
					type: "GET",
					success: function(result){
						var obj = JSON.parse(result);
						$("#id").val(obj['id']);
						$("#pertanyaan").val(obj['pertanyaan']);
						$("#jawaban").val(obj['jawaban']);
						$("#modal-edit").modal('show');
					}
				})
			}
		</script>