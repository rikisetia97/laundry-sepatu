<div class="content-wrapper" style="min-height: 543px;">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active">Ganti Password</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-md-6">
					<div class="card card-secondary">
						<div class="card-header">
							<h3 class="card-title">Pengaturan Website</h3>

							<div class="card-tools">
								<button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
									<i class="fas fa-minus"></i>
								</button>
							</div>
						</div>
						<form method="POST" action="<?=base_url('admin/pengaturan/website_simpan');?>">
							<div class="card-body">
								<div class="form-group">
									<label for="">Nama Website</label>
									<input type="text" class="form-control" name="nama_website" value="<?=$nama_website;?>">
								</div>
								<div class="form-group">
									<label for="">Nomor HP</label>
									<input type="text" class="form-control" name="nomor_hp" value="<?=$nomor_hp;?>">
								</div>

							</div>
							<div class="card-footer">
								<button class="btn btn-primary float-sm-right">Simpan Perubahan</button>
							</div>
						</form>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
				<div class="col-md-6">
					<div class="card card-secondary">
						<div class="card-header">
							<h3 class="card-title">Pengaturan Biaya Tambahan</h3>

							<div class="card-tools">
								<button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
									<i class="fas fa-minus"></i>
								</button>
							</div>
						</div>
						<form method="POST" action="<?=base_url('admin/pengaturan/biaya_simpan');?>">
							<div class="card-body">
								<div class="form-group">
									<label for="">Biaya Antar</label>
									<input type="text" class="form-control" name="biaya_antar" value="<?=$biaya_antar;?>">
								</div>
								<div class="form-group">
									<label for="">Biaya Jemput</label>
									<input type="text" class="form-control" name="biaya_jemput" value="<?=$biaya_jemput;?>">
								</div>
							</div>
							<div class="card-footer">
								<button class="btn btn-primary float-sm-right">Simpan Perubahan</button>
							</div>
						</form>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</section>
		<!-- /.content -->
	</div>

