<div class="content-wrapper" style="min-height: 543px;">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active">Data Kustomer</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-12">
					<div class="card card-secondary">
						<!-- /.card-header -->
						<div class="card-header">
							<h3 class="card-title mt-2">Data Kustomer (Semua)</h3>
							<button class="float-sm-right btn btn-success" data-toggle="modal" data-target="#modal-add"><i class="fas fa-plus"></i> Tambah Kustomer</button>
						</div>
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama</th>
										<th>Alamat</th>
										<th>No. HP</th>
										<th>Tanggal Lahir</th>
										<th>Tanggal Gabung</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($customer as $key) { ?>
										<tr>
											<td><?=$i++;?></td>
											<td><?=$key['nama'];?></td>
											<td><?=$key['alamat'];?></td>
											<td><?=$key['no_hp'];?></td>
											<td><?=$key['tgl_lahir'];?></td>
											<td><?=$key['tgl_gabung'];?></td>
											<td>
												<button class="btn btn-primary" onclick="edit(<?=$key['id'];?>)"><i class="fas fa-edit"></i></button>
												<button class="btn btn-danger" onclick="hapus(<?=$key['id'];?>)"><i class="fas fa-trash"></i></button>
											</td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No.</th>
										<th>Nama</th>
										<th>Alamat</th>
										<th>No. HP</th>
										<th>Tanggal Lahir</th>
										<th>Tanggal Gabung</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.row -->
					</div><!-- /.container-fluid -->
				</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->
		</div>
		<div class="modal fade" id="modal-add" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Tambah Kustomer</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form method="POST" action="<?=base_url('admin/customer/tambah');?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Nama</label>
								<input type="text" class="form-control" name="nama" placeholder="Input Nama Customer" required="required">
							</div>
							<div class="form-group">
								<label for="">Alamat</label>
								<textarea class="form-control" name="alamat" placeholder="Input Alamat Lengkap" required="required"></textarea>
							</div>
							<div class="form-group">
								<label for="">No. Handphone</label>
								<input type="text" class="form-control" name="no_hp" placeholder="Input Nomor HP" required="required">
							</div>
							<div class="form-group">
								<label for="">Tanggal Lahir</label>
								<input type="date" class="form-control" name="tgl_lahir" placeholder="Input Umur Customer" required="required">
							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Simpan Data</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<div class="modal fade" id="modal-edit" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Edit Kustomer</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<form method="POST" action="<?=base_url('admin/customer/edit');?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Nama</label>
								<input type="hidden" id="id" name="id">
								<input type="text" class="form-control" id="nama" name="nama" placeholder="Input Nama Customer" required="required">
							</div>
							<div class="form-group">
								<label for="">Alamat</label>
								<textarea class="form-control" name="alamat" id="alamat" placeholder="Input Alamat Lengkap" required="required"></textarea>
							</div>
							<div class="form-group">
								<label for="">No. Handphone</label>
								<input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="Input Nomor HP" required="required">
							</div>
							<div class="form-group">
								<label for="">Tanggal Lahir</label>
								<input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir" placeholder="Input Umur Customer" required="required">
							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Simpan Perubahan</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<script>
			function hapus(id) {
				Swal.fire({
					title: 'Apakah anda yakin?',
					text: "Anda akan menghapus data kustomer berikut?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Hapus saja!'
				}).then((result) => {
					if (result.isConfirmed) {
						$.ajax({
							url: "customer/hapus_customer/"+id, 
							type: "GET",
							success: function(result){
								Swal.fire(
									'Dihapus!',
									'Data berhasil dihapus.',
									'success'
									).then(function(result) {
										location.reload();
									});
								}
							})
					}
				})
			}

			function edit(id) {
				$.ajax({
					url: "customer/detail_customer/"+id, 
					type: "GET",
					success: function(result){
						var obj = JSON.parse(result);
						$("#id").val(obj['id']);
						$("#nama").val(obj['nama']);
						$("#alamat").val(obj['alamat']);
						$("#no_hp").val(obj['no_hp']);
						$("#tgl_lahir").val(obj['tgl_lahir']);
						$("#modal-edit").modal('show');
					}
				})
			}
		</script>