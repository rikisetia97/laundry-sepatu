<div class="content-wrapper" style="min-height: 543px;">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Dashboard</a></li>
						<li class="breadcrumb-item active">Jasa</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-12">
					<div class="card card-secondary">
						<!-- /.card-header -->
						<div class="card-header">
							<h3 class="card-title mt-2"><i class="fas fa-exchange-alt"></i> Data Pesanan (<?=ucfirst(str_replace('_', ' ', $this->uri->segment(4)));?>)</h3>
							<button class="float-sm-right btn btn-success" data-toggle="modal" data-target="#modal-default"><i class="fas fa-plus"></i> Tambah Pesanan</button>
						</div>
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No.</th>
										<th>Kode Cari</th>
										<th>Pesanan Dibuat</th>
										<th>Nama Kustomer</th>
										<th>No. HP</th>
										<th>Jasa</th>
										<th>Biaya Jasa</th>
										<th>Biaya Tambahan</th>
										<th>Jumlah</th>
										<th>Total</th>
										<th>Status</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($transaksi as $key) { ?>
										<tr>
											<td><?=$i++;?></td>
											<td><?=$key['kode_cari'];?></td>
											<td><?=$key['pesanan_dibuat'];?></td>
											<td><?=$key['nama'];?></td>
											<td><?=$key['no_hp'];?></td>
											<td><?=$key['jasa'];?></td>
											<td>Rp. <?=number_format($key['biaya']);?></td>
											<td>Rp. <?=number_format($key['biaya_tambahan']);?></td>
											<td><?=$key['jumlah'];?></td>
											<td>Rp. <?=number_format($key['total']);?></td>
											<td>
												<span class="badge badge-<?php if($key['status'] == 0) { echo "warning"; } else { echo "success"; } ?>"> <?php if($key['status'] == 0) {echo "Dalam Proses"; } else {echo "Selesai"; } ?> </span>
											</td>
											<td>
												<?php if($key['status'] == 0) { ?> 
													<a href="<?=base_url('admin/transaksi/selesai/'.$key['id']);?>" class="btn btn-success" disabled="disabled"><i class="fas fa-check"></i></a>
												<?php } else { ?> 
													<button class="btn btn-success" disabled="disabled"><i class="fas fa-check"></i></button>
												<?php } ?>
											</td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th>No.</th>
										<th>Kode Cari</th>
										<th>Pesanan Dibuat</th>
										<th>Nama Kustomer</th>
										<th>No. HP</th>
										<th>Jasa</th>
										<th>Biaya Jasa</th>
										<th>Biaya Tambahan</th>
										<th>Jumlah</th>
										<th>Total</th>
										<th>Status</th>
										<th>Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.row -->
					</div><!-- /.container-fluid -->
				</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->
		</div>

		<div class="modal fade" id="modal-default">
			<div class="modal-dialog modal-xl">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Tambah Pesanan Manual</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="<?=base_url('admin/transaksi/simpan');?>" method="POST">
						<div class="modal-body">
							<div class="form-group">
								<label for="">Pilih Kustomer</label>
								<select class="select2 form-control" name="id_customer" style="width: 100%;">
									<option selected="selected" disabled="disabled">Pilih Kustomer</option>
									<?php foreach ($customer as $item) { ?> 
										<option value="<?=$item['id'];?>"><?=$item['nama'];?> - <?=$item['alamat'];?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label for="">Pilih Jasa yang akan digunakan</label>
								<select class="select2 form-control" name="id_jasa" id="id_jasa" style="width: 100%;">
									<option selected="selected" disabled="disabled">Pilih Jasa</option>
									<?php foreach ($jasa as $item) { ?> 
										<option value="<?=$item['id'];?>"><?=$item['jasa'];?> (Rp. <?=number_format($item['biaya']);?>)</option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label for="">Masukkan Jumlah Pakaian/Sepatu</label>
								<input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Masukkan jumlah pakaian/sepatu">
								<input type="hidden" name="total" id="total">
							</div>
							<div class="form-group">
								<label for="">Jenis Layanan Customer</label>
								<select class="form-control" id="jenis_layanan_customer">
									<option value="" selected="selected" disabled="disabled">Pilih Jenis Layanan Customer</option>
									<option value="">Ambil/Jemput Sendiri</option>
									<option value="2">Ambil/Jemput Petugas</option>
									<option value="3">Antar Sendiri (Dijemput Petugas)</option>
									<option value="4">Jemput Sendiri (Diantarkan Petugas)</option>
								</select>
							</div>
							<div class="alert alert-info" id="alert_biaya_Tambahan" style="display: none;">
								<i class="icon fas fa-info-circle"></i> Biaya tambahan untuk jenis layanan dapat diubah di Pengaturan/General.
							</div>
							<input type="hidden" name="biaya_tambahan" id="biaya_tambahan">
							<div class="form-group">
								<label for="">Estimasi Total Pesanan</label>

								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Jasa</th>
											<th>Biaya Jasa</th>
											<th>Jumlah Pakaian/Sepatu</th>
											<th>Biaya Tambahan</th>
											<th>Total Biaya</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td id="text_jasa">NaN</td>
											<td id="text_biaya_jasa">NaN</td>
											<td id="text_jml">NaN</td>
											<td id="text_tambahan">NaN</td>
											<td id="text_total">NaN</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="modal-footer justify-content-between">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary">Simpan Data</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>

		<script>
			var biaya = 0;
			var jml = 0;
			var tambahan = 0;
			var total = 0;
			$("#jumlah").on("keyup change", function() {
				biaya = $("#text_biaya_jasa").html();
				jml = $("#jumlah").val();
				tambahan = $("#biaya_tambahan").val();
				total = biaya * jml + parseInt(tambahan);
				$("#text_jml").html(jml);
				$("#text_total").html(total);
				$("#text_tambahan").html(tambahan);
				$("#total").val(total);
			})
			$("#id_jasa").change(function() {
				var id = $("#id_jasa").val();
				$.ajax({
					url: "../get_jasa/"+id, 
					type: "GET",
					success: function(result){
						var obj = JSON.parse(result);
						$("#text_jasa").html(obj['jasa']);
						$("#text_biaya_jasa").html(obj['biaya']);
						biaya = $("#text_biaya_jasa").html();
						jml = $("#jumlah").val();
						tambahan = $("#biaya_tambahan").val();
						total = biaya * jml + parseInt(tambahan);
						$("#text_jml").html(jml);
						$("#text_tambahan").html(tambahan);
						$("#text_total").html(total);
						$("#total").val(total);
					}
				})
			})
			$("#jenis_layanan_customer").change(function() {
				tambahan = 0;
				var jenis = $("#jenis_layanan_customer").val();
				if (jenis != '') {
					if (jenis == 2) {
						$.ajax({
							url: "../get_pengaturan/biaya_antar", 
							type: "GET",
							success: function(result){
								var obj = JSON.parse(result);
								tambahan += parseInt(obj['value']);
								$('#biaya_tambahan').val(tambahan);
								biaya = $("#text_biaya_jasa").html();
								jml = $("#jumlah").val();
								total = biaya * jml + parseInt(tambahan);
								$("#text_tambahan").html(tambahan);
								$("#text_total").html(total);
								$("#total").val(total);
							}
						})
						$.ajax({
							url: "../get_pengaturan/biaya_jemput", 
							type: "GET",
							success: function(result){
								var obj = JSON.parse(result);
								tambahan += parseInt(obj['value']);
								$('#biaya_tambahan').val(tambahan);
								biaya = $("#text_biaya_jasa").html();
								jml = $("#jumlah").val();
								total = biaya * jml + parseInt(tambahan);
								$("#text_tambahan").html(tambahan);
								$("#text_total").html(total);
								$("#total").val(total);
								
							}
						})
					}else if (jenis == 3) {
						$.ajax({
							url: "../get_pengaturan/biaya_jemput", 
							type: "GET",
							success: function(result){
								var obj = JSON.parse(result);
								tambahan += parseInt(obj['value']);
								$('#biaya_tambahan').val(tambahan);
								biaya = $("#text_biaya_jasa").html();
								jml = $("#jumlah").val();
								total = biaya * jml + parseInt(tambahan);
								$("#text_tambahan").html(tambahan);
								$("#text_total").html(total);
								$("#total").val(total);
							}
						})
					}else if (jenis == 4) {
						$.ajax({
							url: "../get_pengaturan/biaya_antar", 
							type: "GET",
							success: function(result){
								var obj = JSON.parse(result);
								tambahan += parseInt(obj['value']);
								$('#biaya_tambahan').val(tambahan);
								biaya = $("#text_biaya_jasa").html();
								jml = $("#jumlah").val();
								total = biaya * jml + parseInt(tambahan);
								$("#text_tambahan").html(tambahan);
								$("#text_total").html(total);
								$("#total").val(total);
							}
						})
					}

					$('#alert_biaya_Tambahan').css('display', 'block');

				}else{
					biaya = $("#text_biaya_jasa").html();
					jml = $("#jumlah").val();
					$("#biaya_tambahan").val(0);
					tambahan = $("#biaya_tambahan").val();
					total = biaya * jml + parseInt(tambahan);
					$("#text_tambahan").html(tambahan);
					$("#text_total").html(total);
					$("#total").val(total);
					$('#alert_biaya_Tambahan').css('display', 'none');
				}
			})

		</script>