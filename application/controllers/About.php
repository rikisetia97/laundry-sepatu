<?php

class About extends CI_Controller{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{

		$data['_view'] = 'frontend/about';
		$this->load->view('frontend/layouts/main',$data);
	}
}
