<?php

class Dashboard extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Transaksi_model', 'Transaksi');
        $this->load->model('Jasa_model', 'Jasa');
	}

	function index()
	{

        $data['jasa'] = $this->Jasa->get_all_jasa_limit(4);
		$data['_view'] = 'frontend/dashboard';
		$this->load->view('frontend/layouts/main',$data);
	}

	public function cek_status()
	{
		$kode_cari = $this->input->post('kode_cari');
		$data['_view'] = 'frontend/cek_status';
		$data['data'] = $this->Transaksi->cek_transaksi($kode_cari);
		$this->load->view('frontend/layouts/main',$data);
	}

	public function get_transaksi($kode_cari)
	{
		$output = $this->Transaksi->cek_transaksi($kode_cari);
		echo json_encode($output);
	}

}
