<?php

class Faq extends CI_Controller{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$data['faq'] = $this->db->get('tb_faq')->result();
		$data['_view'] = 'frontend/faq';
		$this->load->view('frontend/layouts/main',$data);
	}
}
