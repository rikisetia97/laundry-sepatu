<?php

class Services extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jasa_model', 'Jasa');
        $this->load->model('Transaksi_model', 'Transaksi');
    }

    function index()
    {
        $data['jasa'] = $this->Jasa->get_all_jasa();
        $data['_view'] = 'frontend/services';
        $this->load->view('frontend/layouts/main',$data);
    }
    
    function checkout($id)
    {
        $data['list_jasa'] = $this->Jasa->get_all_jasa();
        $data['jasa'] = $this->Jasa->get_jasa($id);
        $data['_view'] = 'frontend/services_checkout';
        $this->load->view('frontend/layouts/main',$data);
    }

    public function get_pengaturan($jenis)
    {
        $output = $this->db->get_where('tb_pengaturan', ['jenis' => $jenis])->row();
        echo json_encode($output);
    }

    public function save_order()
    {
        $data =$this->input->post();
        if (empty($data['total']) || $data['total'] == 0) {
            $this->session->set_flashdata('failed', 'Transaksi Gagal Disimpan');
        }else{
            $data['id_customer'] = $this->session->userdata('sess_user')['id'];
            $data['kode_cari'] = $this->generateRandomString();
            $insert = $this->Transaksi->add_transaksi($data);
            if ($insert) {
                $this->session->set_flashdata('success', 'Transaksi Berhasil Disimpan');
            }else{
                $this->session->set_flashdata('failed', 'Transaksi Gagal Disimpan');
            }
        }
        redirect('services/riwayat');
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function riwayat()
    {
        if (empty($this->session->userdata('sess_user'))) {
            redirect('.');
        }
        $data['pesanan'] = $this->Transaksi->get_transaksi_user($this->session->userdata('sess_user')['id']);
        $data['_view'] = 'frontend/riwayat';
        $this->load->view('frontend/layouts/main',$data);
    }
}
