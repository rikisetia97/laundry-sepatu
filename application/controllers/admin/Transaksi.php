<?php

class Transaksi extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Transaksi_model', 'Transaksi');
        $this->load->model('Customer_model', 'Customer');
        $this->load->model('Jasa_model', 'Jasa');
        if ($this->session->userdata('sess_admin') == null) {
            redirect('admin/auth/login');
        }
    } 

    function index($status = '')
    {
        if ($status == 'dalam_proses') {
            $data['transaksi'] = $this->Transaksi->get_transaksi_status(0);
        }elseif ($status == 'selesai') {
            $data['transaksi'] = $this->Transaksi->get_transaksi_status(1);
        }else{
            $data['transaksi'] = $this->Transaksi->get_all_transaksi();
        }
        $data['customer'] = $this->Customer->get_all_customer();
        $data['jasa'] = $this->Jasa->get_all_jasa();
        $data['_view'] = 'admin/transaksi';
        $this->load->view('admin/layouts/main',$data);
    }

    public function get_jasa($id)
    {
        $output = $this->Jasa->get_jasa($id);
        echo json_encode($output);
    }

    public function get_pengaturan($jenis)
    {
        $output = $this->db->get_where('tb_pengaturan', ['jenis' => $jenis])->row();
        echo json_encode($output);
    }

    public function simpan()
    {
        $data =$this->input->post();
        if (empty($data['total']) || $data['total'] == 0) {
            $this->session->set_flashdata('failed', 'Transaksi Gagal Disimpan');
        }else{
            $data['kode_cari'] = $this->generateRandomString();
            $insert = $this->Transaksi->add_transaksi($data);
            if ($insert) {
                $this->session->set_flashdata('success', 'Transaksi Berhasil Disimpan');
            }else{
                $this->session->set_flashdata('failed', 'Transaksi Gagal Disimpan');
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function selesai($id)
    {
        $this->Transaksi->update_status($id);
        $this->session->set_flashdata('success', 'Transaksi Telah Selesai');
        redirect($_SERVER['HTTP_REFERER']);

    }

    function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
