<?php

class Auth extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model', 'Auth');
		
	}

	function login()
	{
		if ($this->session->userdata('sess_admin') != null) {
			redirect('admin/dashboard');
		}
		$this->load->view('admin/login');
	}

	function check()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('password','Password','required|max_length[255]');
		$this->form_validation->set_rules('username','Username','required|max_length[30]');
		
		if($this->form_validation->run())     
		{   
			$params = array(
				'password' => $this->input->post('password'),
				'username' => $this->input->post('username'),
			);

			$check = $this->Auth->check_user($params);
			if ($check) {
				$this->session->set_userdata('sess_admin', [
					'id' => $check['id'],
					'username' => $check['username'],
					'nama_lengkap' => $check['nama_lengkap'],
					'created_at' => $check['created_at'],
				]);
				redirect('admin/dashboard/index');
			}else{
				$this->load->view('admin/login');
			}
		}
		else
		{            
			$this->load->view('admin/login');
		}
	}

	function logout()
	{
		$this->session->sess_destroy('sess_admin');
		redirect('admin');
	}
}
