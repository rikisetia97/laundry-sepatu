<?php

class Customer extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model', 'Customer');
        if ($this->session->userdata('sess_admin') == null) {
            redirect('admin/auth/login');
        }
    }

    function index()
    {
        $data['customer'] = $this->Customer->get_all_customer();
        $data['_view'] = 'admin/customer';
        $this->load->view('admin/layouts/main',$data);
    }

    public function hapus_customer($id)
    {
        $output = $this->Customer->delete_customer($id);
        echo json_encode($output);
    }

    public function detail_customer($id)
    {
        $output = $this->Customer->get_customer($id);
        echo json_encode($output);
    }

    public function tambah()
    {
        $input = $this->input->post();
        $insert = $this->Customer->add_customer($input);
        if ($insert) {
            $this->session->set_flashdata('success', 'Kustomer Berhasil Ditambahkan');
        }else{
            $this->session->set_flashdata('failed', 'Kustomer Gagal Ditambahkan');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function edit()
    {
        $input = $this->input->post();
        $id = $input['id'];
        unset($input['id']);
        $insert = $this->Customer->update_customer($id, $input);
        if ($insert) {
            $this->session->set_flashdata('success', 'Kustomer Berhasil Diperbaharui');
        }else{
            $this->session->set_flashdata('failed', 'Kustomer Gagal Diperbaharui');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

}
