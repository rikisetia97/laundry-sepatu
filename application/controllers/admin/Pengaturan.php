<?php

class Pengaturan extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model', 'Admin');
        if ($this->session->userdata('sess_admin') == null) {
            redirect('admin/auth/login');
        }
    } 


    function admin()
    {
        $data['admin'] = $this->Admin->get_all_admin();
        
        $data['_view'] = 'admin/pengaturan_admin';
        $this->load->view('admin/layouts/main',$data);
    }

    function ganti_password()
    {
        $data['_view'] = 'admin/pengaturan_ganti_password';
        $this->load->view('admin/layouts/main',$data);
    }

    function general()
    {
        $data['nama_website'] = $this->db->get_where('tb_pengaturan', ['jenis' => 'nama_website'])->row()->value;
        $data['nomor_hp'] = $this->db->get_where('tb_pengaturan', ['jenis' => 'nomor_hp'])->row()->value;
        $data['biaya_antar'] = $this->db->get_where('tb_pengaturan', ['jenis' => 'biaya_antar'])->row()->value;
        $data['biaya_jemput'] = $this->db->get_where('tb_pengaturan', ['jenis' => 'biaya_jemput'])->row()->value;
        $data['_view'] = 'admin/pengaturan_general';
        $this->load->view('admin/layouts/main',$data);
    }

    function faq()
    {
        $data['faq'] = $this->db->get('tb_faq')->result_array();
        $data['_view'] = 'admin/pengaturan_faq';
        $this->load->view('admin/layouts/main',$data);
    }

    public function website_simpan()
    {
        $nama_website = $this->input->post('nama_website');
        $this->db->where('jenis', 'nama_website');
        $this->db->set('value', $nama_website);
        $this->db->update('tb_pengaturan');
        $nomor_hp = $this->input->post('nomor_hp');
        $this->db->where('jenis', 'nomor_hp');
        $this->db->set('value', $nomor_hp);
        $this->db->update('tb_pengaturan');
        $this->session->set_flashdata('success', 'Pengaturan Website Berhasil Diperbaharui');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function biaya_simpan()
    {
        $biaya_antar = $this->input->post('biaya_antar');
        $this->db->where('jenis', 'biaya_antar');
        $this->db->set('value', $biaya_antar);
        $this->db->update('tb_pengaturan');
        $biaya_jemput = $this->input->post('biaya_jemput');
        $this->db->where('jenis', 'biaya_jemput');
        $this->db->set('value', $biaya_jemput);
        $this->db->update('tb_pengaturan');
        $this->session->set_flashdata('success', 'Pengaturan Biaya Berhasil Diperbaharui');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function detail_admin($id)
    {
        $output = $this->Admin->get_admin($id);
        echo json_encode($output);
    }

    public function tambah_faq()
    {
        $input = $this->input->post();
        $insert = $this->db->insert('tb_faq', $input);
        if ($insert) {
            $this->session->set_flashdata('success', 'FAQ Berhasil Ditambahkan');
        }else{
            $this->session->set_flashdata('failed', 'FAQ Gagal Ditambahkan');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function edit_faq()
    {
        $input = $this->input->post();
        $insert = $this->db->update('tb_faq', $input);
        if ($insert) {
            $this->session->set_flashdata('success', 'FAQ Berhasil Diperbaharui');
        }else{
            $this->session->set_flashdata('failed', 'FAQ Gagal Diperbaharui');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function detail_faq($id)
    {
        $output = $this->db->get_where('tb_faq', ['id' => $id])->row();
        echo json_encode($output);
    }

     public function hapus_faq($id)
    {
        $this->db->where('id', $id);
        $output = $this->db->delete('tb_faq');
        echo json_encode($output);
    }

    public function tambah()
    {
        $input = $this->input->post();
        $insert = $this->Admin->add_admin($input);
        if ($insert) {
            $this->session->set_flashdata('success', 'Admin Berhasil Ditambahkan');
        }else{
            $this->session->set_flashdata('failed', 'Admin Gagal Ditambahkan');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function edit()
    {
        $input = $this->input->post();
        $id = $input['id'];
        unset($input['id']);
        $insert = $this->Admin->update_admin($id, $input);
        if ($insert) {
            $this->session->set_flashdata('success', 'Admin Berhasil Diperbaharui');
        }else{
            $this->session->set_flashdata('failed', 'Admin Gagal Diperbaharui');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function simpan_password()
    {
        $input = $this->input->post();
        if ($input['new_password'] != $input['repeat_new_password']) {
            $this->session->set_flashdata('failed', 'Password Baru Tidak Sama');
        }else{
            $id = $this->session->userdata('sess_admin')['id'];
            $check = $this->Admin->check_password($id, $input['old_password']);
            if ($check) {
                $insert = $this->Admin->update_admin_pass($id, $input['new_password']);
                $this->session->set_flashdata('success', 'Password Berhasil Diperbaharui');
            }else{
                $this->session->set_flashdata('failed', 'Password Lama Anda Salah');
            }
        }
        
        redirect($_SERVER['HTTP_REFERER']);
    }

    
}
