<?php
 
class Jasa extends CI_Controller{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jasa_model', 'Jasa');
            if ($this->session->userdata('sess_admin') == null) {
            redirect('admin/auth/login');
        }
    } 

    function index()
    {
        $data['jasa'] = $this->Jasa->get_all_jasa();
        
        $data['_view'] = 'admin/jasa';
        $this->load->view('admin/layouts/main',$data);
    }


    public function hapus_jasa($id)
    {
        $output = $this->Jasa->delete_jasa($id);
        echo json_encode($output);
    }

    public function detail_jasa($id)
    {
        $output = $this->Jasa->get_jasa($id);
        echo json_encode($output);
    }

    public function tambah()
    {
        $input = $this->input->post();
        $insert = $this->Jasa->add_jasa($input);
        if ($insert) {
            $this->session->set_flashdata('success', 'Jasa Berhasil Ditambahkan');
        }else{
            $this->session->set_flashdata('failed', 'Jasa Gagal Ditambahkan');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function edit()
    {
        $input = $this->input->post();
        $id = $input['id'];
        unset($input['id']);
        $insert = $this->Jasa->update_jasa($id, $input);
        if ($insert) {
            $this->session->set_flashdata('success', 'Jasa Berhasil Diperbaharui');
        }else{
            $this->session->set_flashdata('failed', 'Jasa Gagal Diperbaharui');
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    
}
