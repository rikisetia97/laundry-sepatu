<?php

class Dashboard extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        
    	if ($this->session->userdata('sess_admin') == null) {
    		redirect('admin/auth/login');
    	}
    }

    function index()
    {
        $data['_view'] = 'admin/dashboard';
        $data['pesanan_dalam_proses'] = count($this->db->get_where('tb_transaksi', ['status' => 0])->result());
        $data['pesanan_selesai'] = count($this->db->get_where('tb_transaksi', ['status' => 1])->result());
        $data['jml_jasa'] = count($this->db->get('tb_jasa')->result());
        $data['jml_customer'] = count($this->db->get('tb_customer')->result());
        $this->load->view('admin/layouts/main',$data);
    }
}
