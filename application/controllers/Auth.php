<?php

class Auth extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model', 'Auth');
		$this->load->model('Customer_model', 'Customer');
		
	}

	function check()
	{
		$params = array(
			'no_hp' => $this->input->post('no_hp'),
			'password' => $this->input->post('password'),
		);

		$check = $this->Auth->check_user_2($params);
		if ($check) {
			$this->session->set_userdata('sess_user', [
				'id' => $check['id'],
				'no_hp' => $check['no_hp'],
				'nama' => $check['nama'],
				'tgl_gabung' => $check['tgl_gabung'],
			]);
			redirect($_SERVER['HTTP_REFERER']);
		}else{

		$this->session->set_flashdata('failed', 'Nomor HP / Password tidak ditemukan');
			redirect($_SERVER['HTTP_REFERER']);
		}

	}

	public function register()
	{
		$input = $this->input->post();
		$this->Customer->add_customer($input);
		$this->session->set_flashdata('success', 'Pendaftaran Berhasil silahkan login');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function logout()
	{
		$this->session->sess_destroy('sess_user');
		redirect($_SERVER['HTTP_REFERER']);
	}
}
